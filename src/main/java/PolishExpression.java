import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.ArrayDeque;


public class PolishExpression {
    public static void main(String[] args) {
        PolishExpression polishExpression = new PolishExpression();
        System.out.println(polishExpression.reverseCalculator("10 10 10"));
    }

    public Long reverseCalculator(String s) throws IllegalArgumentException {
        ArrayDeque<Long> longs = new ArrayDeque<>();
        String[] strings = s.split(" ");
        Long counter = Long.valueOf(0);
        for (int i = 0; i < strings.length; i++) {
            if (!(strings[i].equals("+") | strings[i].equals("-") | strings[i].equals("*") | strings[i].equals("/"))) {
                longs.addLast(Long.parseLong(strings[i]));
            } else {
                counter = longs.peekLast();
                longs.pollLast();
                if (strings[i].equals("+")) {

                    check(longs, counter);
                }
                if (strings[i].equals("-")) {
                    check(longs, counter);

                }
                if (strings[i].equals("*")) {
                    check(longs, counter);
                }
                if (strings[i].equals("/")) {
                    check(longs, counter);

                }


                longs.pollLast();
                longs.addLast(counter);
                longs.pop();

            }
            if (longs.isEmpty()) {
                return counter;
            } else {
                throw new IllegalArgumentException();
            }

        }
        return counter;
    }

    void check(ArrayDeque<Long> longs,  Long counter) {
        try {
            counter = longs.peekLast() + counter;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }

}



